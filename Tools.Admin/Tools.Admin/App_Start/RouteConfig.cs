﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Tools.Admin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "GetNavs",
                url: "api/getnavs",
                defaults: new { controller = "Home", action = "GetNavs"}
            );
            routes.MapRoute(
                name: "InitData",
                url: "api/init",
                defaults: new { controller = "Home", action = "InitData" }
            );
            routes.MapRoute(
                name: "DeleteData",
                url: "api/delete",
                defaults: new { controller = "Home", action = "Delete" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
