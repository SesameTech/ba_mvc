﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tools.Admin.Controllers
{
    public class ToolsControllerBase:Controller
    {
        /// <summary>
        /// BTable组件专用
        /// </summary>
        /// <param name="rel"></param>
        /// <param name="msg"></param>
        /// <param name="list"></param>
        /// <param name="count"></param>
        /// <returns></returns>

        protected JsonResult JsonResultForTable(bool rel, string msg, object list = null, long count = 0)
        {
            if (!rel)
            {
                return Json(new
                {
                    rel = false,
                    msg = msg
                });
            }
            return Json(new
            {
                rel = true,
                msg = msg,
                list = list,
                count = count
            });

        }
        protected JsonResult JsonResult(bool rel, string msg, object data = null)
        {
            if (Request.HttpMethod.ToUpper() == "GET")
            {
                return Json(GetResult(rel, msg, data), JsonRequestBehavior.AllowGet);
            }
            return Json(GetResult(rel, msg, data));
        }
        private static Dictionary<string, object> GetResult(bool rel, string msg, object data = null)
        {
            var d = new Dictionary<string, object>
            {
                { "rel",rel },
                { "msg",msg}
            };
            if (data != null)
                d.Add("data", data);

            return d;
        }
    }
}